package clases;

public class SesionIndividual extends Sesion {

	/**
	 * @author Daniel C�rdenas Perales
	 */

	/**
	 * Atributos de la sesi�n Individual, todos con el modificador de visibilidad
	 * private, para sacar o cambiar los valores se necesitara utilizar los metodos
	 * getter and setter. sesionDomicilio y esPrime = boolean direccionCliente =
	 * String
	 */

	private boolean sesionDomicilio;
	private String direccionCliente;
	private boolean esPrime;

	/**
	 * hacemos una constante del descuento si un cliente es prime, se le rebaja un
	 * 20% al precio total
	 */

	public final float DESCUENTO_PRIME = 0.2f;

	/**
	 * Creamos el constructor de sesionIndividual, con atributos especificos como
	 * sesionDomicilio y esPrime, ademas tendremos atributos de la super clase que
	 * se encuentran dentro de la palabra super que hace referencia al objeto actual
	 * de la super clase que ser�a Sesion
	 * 
	 * @param nombre          nombre de la sesi�n
	 * @param estado          estado de la sesi�n
	 * @param precio          precio de la sesi�n
	 * @param nombreFisio     nombre del encargado de hacer la sesi�n
	 * @param cupon           si tiene o no cupon la sesi�n
	 * @param cliente         cliente asignado a la sesi�n
	 * @param sesionDomicilio si es o no una sesi�n a domicilio
	 */
	public SesionIndividual(String nombre, String estado, float precio, String nombreFisio, boolean cupon,
			boolean sesionDomicilio, boolean esPrime) {
		super(nombre, estado, precio, nombreFisio, cupon);
		this.sesionDomicilio = sesionDomicilio;
		this.esPrime = esPrime;
	}

	/**
	 * devuelve el valor que puede ser true o false de si es una sesi�n a domicilio
	 * 
	 * @return sesionDomicilio
	 */

	public boolean isSesionDomicilio() {
		return sesionDomicilio;
	}

	/**
	 * cambia el valor si es una sesion a domicilio o no
	 * 
	 * @param sesionDomicilio
	 */

	public void setSesionDomicilio(boolean sesionDomicilio) {
		this.sesionDomicilio = sesionDomicilio;
	}

	/**
	 * Devuelve la direccion del cliente
	 * 
	 * @return direccionCliente
	 */

	public String getDireccionCliente() {
		return direccionCliente;
	}

	/**
	 * Cambia el valor de la direccion del cliente
	 * 
	 * @param direccionCliente
	 */

	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}

	/**
	 * Devuelve si es o no un cliente prime
	 * 
	 * @return esPrime
	 */

	public boolean isEsPrime() {
		return esPrime;
	}

	/**
	 * Cambia el valor del atributo esPrime, solo se puede cambiar de valores como
	 * true o false
	 * 
	 * @param esPrime
	 */

	public void setEsPrime(boolean esPrime) {
		this.esPrime = esPrime;
	}

	/**
	 * Si es un cliente prime se le rebajara al total un 20%.
	 */

	public void precioRebajado() {
		float descuento = this.precio * DESCUENTO_PRIME;
		this.precio -= descuento;
	}

	/**
	 * metodo que devuelve todos los valores de los atributos de una sesi�n
	 * individual
	 */
	@Override
	public String toString() {
		return "SesionIndividual [sesionDomicilio=" + sesionDomicilio + ", direccionCliente=" + direccionCliente
				+ ", esPrime=" + esPrime + ", DESCUENTO_PRIME=" + DESCUENTO_PRIME + ", nombre=" + nombre + ", estado="
				+ estado + ", precio=" + precio + ", nombreFisio=" + nombreFisio + ", cupon=" + cupon + ", cliente="
				+ cliente + "]";
	}

}
