package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorSesionCliente {

	/**
	 * @author Daniel C�rdenas Perales
	 */

	/**
	 * Creamos los objetos de los ArrayList de cada una de las clases creadas
	 * anteriormente. Cliente -> listadoClientes Sesion -> listadoSesiones
	 * SesionIndividual -> listadoSesionesIndividuales SesionGrupal ->
	 * listadoSesionesGrupales
	 */

	private ArrayList<Cliente> listadoClientes;
	private ArrayList<Sesion> listadoSesiones;
	private ArrayList<SesionIndividual> listadoSesionesIndividuales;
	private ArrayList<SesionGrupal> listadoSesionesGrupales;

	/**
	 * Creamos el constructor de gestorSesionCliente, inicializando todos los
	 * arraylist creados.
	 */
	public GestorSesionCliente() {
		this.listadoClientes = new ArrayList<Cliente>();
		this.listadoSesiones = new ArrayList<Sesion>();
		this.listadoSesionesIndividuales = new ArrayList<SesionIndividual>();
		this.listadoSesionesGrupales = new ArrayList<SesionGrupal>();
	}

	/******************************************************************************************************/
	// CLIENTE

	/**
	 * Metodo el cual su funci�n es dar de alta un cliente, comprobaremos si el
	 * cliente existe, si el cliente existe, nos mostrara un mensaje diciendo que el
	 * cliente ya existe
	 * 
	 * @param nombre          nombre del cliente
	 * @param email           email del cliente
	 * @param fechaNacimiento fecha de nacimiento del cliente
	 * @param usuario         usuario del cliente
	 * @param contrasena      constrase�a del cliente
	 * @param numeroSesiones  numero de sesiones del cliente
	 * @return void
	 */

	public void altaCliente(String nombre, String email, String fechaNacimiento, String usuario, String contrasena,
			int numeroSesiones) {
		if (!existeCliente(usuario)) {
			Cliente clienteNuevo = new Cliente(nombre, usuario, contrasena);
			clienteNuevo.setEmail(email);
			clienteNuevo.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
			clienteNuevo.setActivo(true);
			clienteNuevo.setNumeroSesiones(numeroSesiones);
			listadoClientes.add(clienteNuevo);
			System.out.println("El cliente: " + clienteNuevo.getNombre() + " ha sido a�adido");
		} else {
			System.out.println("Ese cliente ya existe");
		}
	}

	/**
	 * metodo que comprueba si existe un cliente o no, mediante un if comprobando si
	 * el cliente es lo contrario de nulo y tambien comprobando el usuario que tiene
	 * asignado el cliente con el que usuario escrito en el menu al llamar al metodo
	 * 
	 * @param usuario usuario del cliente
	 * @return boolean para comfirmar la existencia del cliente
	 */

	public boolean existeCliente(String usuario) {
		for (Cliente cliente : listadoClientes) {
			if (cliente != null && cliente.getUsuario().equals(usuario)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * metodo para mostrar todos los clientes que han sido dados de alta, si no hay
	 * ning�n cliente dado en alta nos mostrara un mensaje comentado que no hay
	 * clientes registrados
	 */

	public void listarCliente() {
		for (Cliente cliente : listadoClientes) {
			System.out.println(cliente);
		}

		if (listadoClientes.size() == 0) {
			System.out.println("No hay clientes registrados");
		}
	}

	/**
	 * metodo el cual nos busca el cliente ingresando el usuario en el men� al
	 * llamar al metodo, si no encuentra dicho cliente nos devuelve un nulo
	 * 
	 * @param usuario usuario del cliente buscado
	 * @return Cliente objeto del cliente
	 */

	public Cliente buscarCliente(String usuario) {
		for (int i = 0; i < listadoClientes.size(); i++) {
			if (listadoClientes.get(i).getUsuario().equals(usuario)) {
				return listadoClientes.get(i);
			}
		}
		return null;
	}

	/**
	 * metodo que se utiliza para podre eliminar un cliente en el arraylist, para
	 * ello al llamar al metodo nos dara un usuario que buscaremos en nuestro
	 * arrayList, si no lo encuentra nos mostrara un mensaje diciendo que no existe
	 * el usuario del cliente
	 * 
	 * @param usuario usuario del cliente a eliminar return void
	 */

	public void eliminarCliente(String usuario) {
		Iterator<Cliente> iteradorCliente = listadoClientes.iterator();
		int contador = 0;
		while (iteradorCliente.hasNext()) {
			Cliente cliente = (Cliente) iteradorCliente.next();
			if (cliente.getUsuario().equals(usuario)) {
				contador++;
				iteradorCliente.remove();
			}
		}
		if (contador == 0) {
			System.out.println("No existe el usuario del cliente");
		}
	}

	/******************************************************************************************************/
	// ALTA DE SESIONES

	/**
	 * metodo en el cual a�adiremos una sesi�n a nuestro arrayList llamado:
	 * listadoSesiones, si el nombre ingresado ya existe nos mostrara un mensaje
	 * diciendo que la sesi�n ya esta creada
	 * 
	 * @param nombre      nombre de la sesi�n
	 * @param estado      estado de la sesi�n
	 * @param precio      precio de la sesi�n
	 * @param nombreFisio nombre del fisio que realizara la sesi�n
	 * @param cupon       booleano diciendo si tiene o no cupon
	 */

	public void altaSesion(String nombre, String estado, float precio, String nombreFisio, boolean cupon) {
		if (!existeSesion(nombre)) {
			Sesion sesion = new Sesion(nombre, estado);
			sesion.setPrecio(precio);
			sesion.setNombreFisio(nombreFisio);
			sesion.setCupon(cupon);
			listadoSesiones.add(sesion);
		} else {
			System.out.println("Esa sesi�n ya existe");
		}
	}

	/**
	 * metodo en el cual a�adiremos una sesi�n Individual a nuestro arrayList
	 * llamado: listadoSesionesIndividuales, si el nombre ingresado ya existe nos
	 * mostrara un mensaje diciendo que la sesi�n individual ya esta creada, si la
	 * sesi�n es prime llamaremos al metodo precio rebajado
	 * 
	 * @param nombre          nombre de la sesi�n
	 * @param estado          estado de la sesi�n
	 * @param precio          precio de la sesi�n
	 * @param nombreFisio     nombre del fisio que realizara la sesi�n
	 * @param cupon           booleano diciendo si tiene o no cupon
	 * @param sesionDomicilio booleano diciendo si la sesi�n es a domicilio o no
	 * @param esPrime         booleano diciendo si la sesi�n es priime o no
	 */

	public void altaSesionIndividual(String nombre, String estado, float precio, String nombreFisio, boolean cupon,
			boolean sesionDomicilio, boolean esPrime) {
		if (!existeSesionIndividual(nombre)) {
			SesionIndividual sesionIndividual = new SesionIndividual(nombre, estado, precio, nombreFisio, cupon,
					sesionDomicilio, esPrime);
			if (esPrime) {
				sesionIndividual.precioRebajado();
			}
			listadoSesionesIndividuales.add(sesionIndividual);
		} else {
			System.out.println("Esa sesi�n ya existe");
		}
	}

	/**
	 * metodo en el cual a�adiremos una sesi�n Individual a nuestro arrayList
	 * llamado: listadoSesionesIndividuales, si el nombre ingresado ya existe nos
	 * mostrara un mensaje diciendo que la sesi�n individual ya esta creada, si la
	 * sesi�n es prime llamaremos al metodo precio rebajado, si es una sesi�n a
	 * domicilio se llamara a este metodo, para poder a�adir la direcci�n del
	 * cliente
	 * 
	 * @param nombre          nombre de la sesi�n
	 * @param estado          estado de la sesi�n
	 * @param precio          precio de la sesi�n
	 * @param nombreFisio     nombre del fisio que realizara la sesi�n
	 * @param cupon           booleano diciendo si tiene o no cupon
	 * @param sesionDomicilio booleano diciendo si la sesi�n es a domicilio o no
	 * @param esPrime         booleano diciendo si la sesi�n es priime o no
	 * @param direccion       direcci�n del cliente
	 */

	public void altaSesionIndividual(String nombre, String estado, float precio, String nombreFisio, boolean cupon,
			boolean sesionDomicilio, boolean esPrime, String direccion) {
		if (!existeSesionIndividual(nombre)) {
			SesionIndividual sesionIndividual = new SesionIndividual(nombre, estado, precio, nombreFisio, cupon,
					sesionDomicilio, esPrime);
			if (sesionDomicilio) {
				sesionIndividual.setDireccionCliente(direccion);
			}
			if (esPrime) {
				sesionIndividual.precioRebajado();
			}
			listadoSesionesIndividuales.add(sesionIndividual);
		} else {
			System.out.println("Esa sesi�n ya existe");
		}
	}

	/**
	 * metodo en el cual a�adiremos una sesi�n grupal a nuestro arrayList llamado:
	 * listadoSesionesGrupales, si el nombre ingresado ya existe nos mostrara un
	 * mensaje diciendo que la sesi�n ya esta creada
	 * 
	 * @param nombre                nombre de la sesi�n
	 * @param estado                estado de la sesi�n
	 * @param precio                precio de la sesi�n
	 * @param nombreFisio           nombre del fisio que realizara la sesi�n
	 * @param cupon                 booleano diciendo si tiene o no cupon
	 * @param segundoNombreFisio    nombre del segundo fisio que realizara la sesi�n
	 * @param aforoMaximoSala       Aforo m�ximo de la sala
	 * @param numeroMaterialesTotal Numero de materiales para realizar la sesi�n
	 */

	public void altaSesionGrupal(String nombre, String estado, float precio, String nombreFisio, boolean cupon,
			String segundoNombreFisio, int aforoMaximoSala, int numeroMaterialesTotal) {
		if (!existeSesionGrupal(nombre)) {
			SesionGrupal sesionGrupal = new SesionGrupal(nombre, estado, precio, nombreFisio, cupon, segundoNombreFisio,
					aforoMaximoSala, numeroMaterialesTotal);
			listadoSesionesGrupales.add(sesionGrupal);
		} else {
			System.out.println("Esa sesi�n ya existe");
		}
	}

	/******************************************************************************************************/
	// LISTAR SESIONES

	/**
	 * metodo para listar las sesiones, si no existe ninguna sesion, nos mostrara un
	 * mensaje indicado en el metodo
	 */
	public void listarSesion() {
		int contador = 0;
		for (Sesion sesiones : listadoSesiones) {
			contador++;
			System.out.println(sesiones);
		}

		if (contador == 0) {
			System.out.println("No hay sesiones registradas");
		}
	}

	/**
	 * metodo para listar las sesiones individuales, si no existe ninguna sesion
	 * individual, nos mostrara un mensaje indicado en el metodo
	 */
	public void listarSesionIndividual() {
		int contador = 0;
		for (Sesion sesiones : listadoSesionesIndividuales) {
			contador++;
			System.out.println(sesiones);
		}

		if (contador == 0) {
			System.out.println("No hay sesiones registradas");
		}
	}

	/**
	 * metodo para listar las sesiones grupales, si no existe ninguna sesion grupal,
	 * nos mostrara un mensaje indicado en el metodo
	 */
	public void listarSesionGrupal() {
		int contador = 0;
		for (Sesion sesiones : listadoSesionesGrupales) {
			contador++;
			System.out.println(sesiones);
		}

		if (contador == 0) {
			System.out.println("No hay sesiones registradas");
		}
	}

	/******************************************************************************************************/
	// BUSCAR SESIONES

	/**
	 * metodo en el cual nos buscara la sesi�n en el arraylist, al llamar al metodo
	 * nos dara un String que hace referencia al nombre, si no lo encuentra nos
	 * mostrara un valor nulo
	 * 
	 * @param nombre nombre de la sesi�n
	 * @return Sesion un objeto que hace referencia a la sesi�n buscada
	 */
	public Sesion buscarSesion(String nombre) {
		for (int i = 0; i < listadoSesiones.size(); i++) {
			if (listadoSesiones.get(i).getNombre().equals(nombre)) {
				return listadoSesiones.get(i);
			}
		}
		return null;
	}

	/**
	 * metodo en el cual nos buscara la sesi�n individual en el arraylist, al llamar
	 * al metodo nos dara un String que hace referencia al nombre, si no lo
	 * encuentra nos mostrara un valor nulo
	 * 
	 * @param nombre nombre de la sesi�n individual
	 * @return SesionIndividual un objeto que hace referencia a la sesi�n buscada
	 */

	public SesionIndividual buscarSesionIndividual(String nombre) {
		for (int i = 0; i < listadoSesionesIndividuales.size(); i++) {
			if (listadoSesionesIndividuales.get(i).getNombre().equals(nombre)) {
				return listadoSesionesIndividuales.get(i);
			}
		}
		return null;
	}

	/**
	 * metodo en el cual nos buscara la sesi�n grupal en el arraylist, al llamar al
	 * metodo nos dara un String que hace referencia al nombre, si no lo encuentra
	 * nos mostrara un valor nulo
	 * 
	 * @param nombre nombre de la sesi�n grupal
	 * @return SesionGrupal un objeto que hace referencia a la sesi�n buscada
	 */

	public SesionGrupal buscarSesionGrupal(String nombre) {
		for (int i = 0; i < listadoSesionesGrupales.size(); i++) {
			if (listadoSesionesGrupales.get(i).getNombre().equals(nombre)) {
				return listadoSesionesGrupales.get(i);
			}
		}
		return null;
	}

	/******************************************************************************************************/
	// ELIMINAR SESIONES

	/**
	 * metodo en el cual nos eliminara una sesi�n, al llamar al metodo nos dara el
	 * nombre que hara referencia al nombre de la sesi�n que tendremos que eliminar,
	 * si no encuentra la sesi�n, nos devolvera un mensaje
	 * 
	 * @param nombre nombre de la sesi�n
	 */

	public void eliminarSesion(String nombre) {
		Iterator<Sesion> iterador = listadoSesiones.iterator();
		int contador = 0;
		while (iterador.hasNext()) {
			Sesion sesion = (Sesion) iterador.next();
			if (sesion.getNombre().equals(nombre)) {
				contador++;
				iterador.remove();
			}
		}
		if (contador == 0) {
			System.out.println("No existe la sesi�n");
		}
	}

	/**
	 * metodo en el cual nos eliminara una sesi�n individual, al llamar al metodo
	 * nos dara el nombre que hara referencia al nombre de la sesi�n individual que
	 * tendremos que eliminar, si no encuentra la sesi�n, nos devolvera un mensaje
	 * 
	 * @param nombre nombre de la sesi�n
	 */

	public void eliminarSesionIndivual(String nombre) {
		Iterator<SesionIndividual> iterador = listadoSesionesIndividuales.iterator();
		int contador = 0;
		while (iterador.hasNext()) {
			Sesion sesion = (Sesion) iterador.next();
			if (sesion.getNombre().equals(nombre)) {
				contador++;
				iterador.remove();
			}
		}
		if (contador == 0) {
			System.out.println("No existe la sesi�n individual");
		}
	}

	/**
	 * metodo en el cual nos eliminara una sesi�n grupal, al llamar al metodo nos
	 * dara el nombre que hara referencia al nombre de la sesi�n grupal que
	 * tendremos que eliminar, si no encuentra la sesi�n, nos devolvera un mensaje
	 * 
	 * @param nombre nombre de la sesi�n
	 */

	public void eliminarSesionGrupal(String nombre) {
		Iterator<SesionGrupal> iterador = listadoSesionesGrupales.iterator();
		int contador = 0;
		while (iterador.hasNext()) {
			Sesion sesion = (Sesion) iterador.next();
			if (sesion.getNombre().equals(nombre)) {
				contador++;
				iterador.remove();
			}
		}
		if (contador == 0) {
			System.out.println("No existe la sesi�n");
		}
	}

	/*************************************************************************************************************/
	// EXISTE SESION

	/**
	 * metodo el cual nos devuelve un booleano con la comprobaci�n de si existe o no
	 * la sesi�n
	 * 
	 * @param nombre nombre de la sesi�n
	 * @return boolean true o false dependiendo de si existe o no
	 */

	public boolean existeSesion(String nombre) {
		for (Sesion sesion : listadoSesiones) {
			if (sesion != null && sesion.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * metodo el cual nos devuelve un booleano con la comprobaci�n de si existe o no
	 * la sesi�n individual
	 * 
	 * @param nombre nombre de la sesi�n individual
	 * @return boolean true o false dependiendo de si existe o no
	 */

	public boolean existeSesionIndividual(String nombre) {
		for (SesionIndividual sesion : listadoSesionesIndividuales) {
			if (sesion != null && sesion.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * metodo el cual nos devuelve un booleano con la comprobaci�n de si existe o no
	 * la sesi�n grupal
	 * 
	 * @param nombre nombre de la sesi�n grupal
	 * @return boolean true o false dependiendo de si existe o no
	 */

	public boolean existeSesionGrupal(String nombre) {
		for (SesionGrupal sesion : listadoSesionesGrupales) {
			if (sesion != null && sesion.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/*************************************************************************************************************/

	/**
	 * metodo en el cual asignaremos un cliente a una sesi�n, comprobaremos tanto si
	 * el cliente existe como la sesi�n el cual queremos asignar.
	 * 
	 * @param usuario usuario del cliente
	 * @param nombre  nobmre de la sesi�n
	 */

	public void asignarSesionCliente(String usuario, String nombre) {
		Cliente cliente = buscarCliente(usuario);
		Sesion sesion = buscarSesion(nombre);
		if (buscarCliente(usuario) != null && buscarSesion(nombre) != null) {
			sesion.setCliente(cliente);
			System.out.println(
					"El cliente " + cliente.getNombre() + " ha sido asignado a la sesi�n: " + sesion.getNombre());
		} else if (buscarCliente(usuario) == null && buscarSesion(nombre) != null) {
			System.out.println("No existe el usuario");
		} else if (buscarSesion(nombre) == null && buscarCliente(usuario) != null) {
			System.out.println("No existe la sesi�n");
		} else {
			System.out.println("No existe ni el cliente ni la sesi�n");
		}
	}

	/**
	 * metodo en el cual asignaremos un cliente a una sesi�n individual,
	 * comprobaremos tanto si el cliente existe como la sesi�n individual el cual
	 * queremos asignar.
	 * 
	 * @param usuario usuario del cliente
	 * @param nombre  nobmre de la sesi�n individual
	 */

	public void asignarSesionIndividualCliente(String usuario, String nombre) {
		Cliente cliente = buscarCliente(usuario);
		SesionIndividual sesionIndividual = buscarSesionIndividual(nombre);
		if (buscarCliente(usuario) != null && buscarSesionIndividual(nombre) != null) {
			sesionIndividual.setCliente(cliente);
		} else if (buscarCliente(usuario) == null && buscarSesionIndividual(nombre) != null) {
			System.out.println("No existe el usuario");
		} else if (buscarSesionIndividual(nombre) == null && buscarCliente(usuario) != null) {
			System.out.println("No existe la sesi�n");
		} else {
			System.out.println("No existe el usuario ni la sesi�n");
		}
	}

	/**
	 * metodo en el cual asignaremos un cliente a una sesi�n grupal, comprobaremos
	 * tanto si el cliente existe como la sesi�n grupal el cual queremos asignar.
	 * 
	 * @param usuario usuario del cliente
	 * @param nombre  nobmre de la sesi�n grupal
	 */

	public void asignarSesionGrupalCliente(String usuario, String nombre) {
		Cliente cliente = buscarCliente(usuario);
		SesionGrupal sesionGrupal = buscarSesionGrupal(nombre);
		if (buscarCliente(usuario) != null && buscarSesionGrupal(nombre) != null) {
			sesionGrupal.altaClienteSesionGrupal(cliente);
		} else if (buscarCliente(usuario) == null && buscarSesionGrupal(nombre) != null) {
			System.out.println("No existe el usuario");
		} else if (buscarSesionGrupal(nombre) == null && buscarCliente(usuario) != null) {
			System.out.println("No existe la sesi�n");
		} else {
			System.out.println("No existe el usuario ni la sesi�n");
		}
	}

	/*************************************************************************************************/

	/**
	 * metodo que nos mostrara todos los clientes que han sido asignados a una
	 * sesi�n grupal
	 * 
	 * @param nombre nombre de la sesi�n grupal
	 */

	public void listarClientesSesionGrupal(String nombre) {
		for (SesionGrupal sesion : listadoSesionesGrupales) {
			if (sesion.getNombre().equals(nombre)) {
				sesion.listarClienteSesionGrupal();
			}
		}
	}

	/*************************************************************************************************/

	/**
	 * metodo que nos mostrara las plazas disponibles que hay en una sesi�n grupal
	 * 
	 * @param nombre nombre de la sesi�n grupal
	 */

	public void clientesDisponiblesPorSesionGrupal(String nombre) {
		for (SesionGrupal sesion : listadoSesionesGrupales) {
			if (sesion.getNombre().equals(nombre)) {
				sesion.numeroClientesSesion();
			}
		}
	}

	/*************************************************************************************************/

	/**
	 * metodo que su funci�n es subir el precio total de una sesi�n, al llamar al
	 * metodo nos dara el nombre de la sesi�n y el precio que queremos subir al
	 * total.
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param precio precio que queremos a�adir a la sesi�n
	 */

	public void subirPrecioSesion(String nombre, float precio) {
		int contador = 0;
		for (Sesion sesion : listadoSesiones) {
			if (existeSesion(nombre) && sesion.getNombre().equals(nombre)) {
				sesion.subidaPrecio(precio);
				System.out.println("El precio ha subido a: " + sesion.getPrecio());
				contador++;
			}
		}

		if (contador == 0) {
			System.out.println("No existe la sesi�n");
		}
	}

	/**
	 * metodo que su funci�n es bajar el precio total de una sesi�n, al llamar al
	 * metodo nos dara el nombre de la sesi�n y el precio que queremos bajar al
	 * total.
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param precio precio que queremos a�adir a la sesi�n para bajar el precio
	 *               total
	 */

	public void bajarPrecioSesion(String nombre, float precio) {
		int contador = 0;
		for (Sesion sesion : listadoSesiones) {
			if (existeSesion(nombre) && sesion.getNombre().equals(nombre)) {
				sesion.bajadaPrecio(precio);
				System.out.println("El precio ha bajado a: " + sesion.getPrecio());
				contador++;
			}
		}

		if (contador == 0) {
			System.out.println("No existe la sesi�n");
		}
	}

	/**
	 * metodo que su funci�n es subir el precio total de una sesi�n individual, al
	 * llamar al metodo nos dara el nombre de la sesi�n individual y el precio que
	 * queremos subir al total.
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param precio precio que queremos a�adir a la sesi�n
	 */

	public void subirPrecioSesionIndividual(String nombre, float precio) {
		int contador = 0;
		for (SesionIndividual sesion : listadoSesionesIndividuales) {
			if (existeSesionIndividual(nombre) && sesion.getNombre().equals(nombre)) {
				sesion.subidaPrecio(precio);
				System.out.println("El precio ha subido a: " + sesion.getPrecio());
				contador++;
			}
		}

		if (contador == 0) {
			System.out.println("No existe la sesi�n individual");
		}
	}

	/**
	 * metodo que su funci�n es bajar el precio total de una sesi�n individual, al
	 * llamar al metodo nos dara el nombre de la sesi�n individual y el precio que
	 * queremos bajar al total.
	 * 
	 * @param nombre nombre de la sesi�n individual
	 * @param precio precio que queremos a�adir a la sesi�n para bajar el precio
	 *               total
	 */

	public void bajarPrecioSesionIndividual(String nombre, float precio) {
		int contador = 0;
		for (SesionIndividual sesion : listadoSesionesIndividuales) {
			if (existeSesionIndividual(nombre) && sesion.getNombre().equals(nombre)) {
				sesion.bajadaPrecio(precio);
				System.out.println("El precio ha bajado a: " + sesion.getPrecio());
				contador++;
			}
		}

		if (contador == 0) {
			System.out.println("No existe la sesi�n individual");
		}
	}

	/**
	 * metodo que su funci�n es subir el precio total de una sesi�n grupal, al
	 * llamar al metodo nos dara el nombre de la sesi�n grupal y el precio que
	 * queremos subir al total.
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param precio precio que queremos a�adir a la sesi�n
	 */

	public void subirPrecioSesionGrupal(String nombre, float precio) {
		int contador = 0;
		for (SesionGrupal sesion : listadoSesionesGrupales) {
			if (existeSesionGrupal(nombre) && sesion.getNombre().equals(nombre)) {
				sesion.subidaPrecio(precio);
				System.out.println("El precio ha subido a: " + sesion.getPrecio());
				contador++;
			}
		}

		if (contador == 0) {
			System.out.println("No existe la sesi�n grupal");
		}
	}

	/**
	 * metodo que su funci�n es bajar el precio total de una sesi�n grupal, al
	 * llamar al metodo nos dara el nombre de la sesi�n grupal y el precio que
	 * queremos bajar al total.
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param precio precio que queremos a�adir a la sesi�n grupal para bajar el
	 *               precio total
	 */

	public void bajarPrecioSesionGrupal(String nombre, float precio) {
		int contador = 0;
		for (SesionGrupal sesion : listadoSesionesGrupales) {
			if (existeSesionGrupal(nombre) && sesion.getNombre().equals(nombre)) {
				sesion.bajadaPrecio(precio);
				System.out.println("El precio ha bajado a: " + sesion.getPrecio());
				contador++;
			}
		}

		if (contador == 0) {
			System.out.println("No existe la sesi�n grupal");
		}
	}

}
