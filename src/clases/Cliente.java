package clases;

import java.time.LocalDate;

public class Cliente {

	/**
	 * @author Daniel Cárdenas Perales
	 */

	/**
	 * Atributos del cliente, todos con el modificador de visibilidad private, para
	 * sacar o cambiar los valores se necesitara utilizar los metodos getter and
	 * setter.
	 */

	private String nombre;
	private String email;
	private LocalDate fechaNacimiento;
	private String usuario;
	private String contrasena;
	private boolean activo;
	private int numeroSesiones;

	/**
	 * Constructor de cliente, necesario introducir el nombre, el usuario y la
	 * contraseña para dar de alta un cliente.
	 * 
	 * @param nombre     nombre del cliente
	 * @param usuario    usuario del cliente
	 * @param contrasena contraseña del cliente
	 */

	public Cliente(String nombre, String usuario, String contrasena) {
		this.nombre = nombre;
		this.usuario = usuario;
		this.contrasena = contrasena;
	}

	/**
	 * metodo para mostrar el nombre
	 * 
	 * @return nombre del cliente
	 */

	public String getNombre() {
		return nombre;
	}

	/**
	 * metodo para cambiar el nombre
	 * 
	 * @param nombre nombre del cliente
	 */

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * metodo para mostrar el correo eléctronico
	 * 
	 * @return email correo eléctronico del cliente
	 */

	public String getEmail() {
		return email;
	}

	/**
	 * metodo para cambiar el correo eléctronico
	 * 
	 * @param email correo eléctronico del cliente
	 */

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * metodo para mostrar la fecha de nacimiento del cliente
	 * 
	 * @return fechaNacimiento fecha de nacimiento del cliente
	 */
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * metodo para cambiar la fecha de nacimiento
	 * 
	 * @param fechaNacimiento fecha de nacimiento del cliente
	 */

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	/**
	 * metodo para mostrar el usuario
	 * 
	 * @return usuario usuario del cliente
	 */

	public String getUsuario() {
		return usuario;
	}

	/**
	 * metodo para cambiar el valor del usuario
	 * 
	 * @param usuario usuario del cliente
	 */

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * metodo para obtener la contraseña
	 * 
	 * @return contrasena contrasena del cliente
	 */

	public String getContrasena() {
		return contrasena;
	}

	/**
	 * metodo para cambiar el valor de una contraseña
	 * 
	 * @param contrasena contrasña del cliente
	 */

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	/**
	 * metodo para comprobar si esta activo el cliente
	 * 
	 * @return activo booleano activo del cliente
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * metodo que devuelve si el cliente esta activo o no
	 * 
	 * @param activo activo del cliente
	 */

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * metodo para mostrar el número de sesiones que ha realizado el cliente
	 * 
	 * @return numeroSesiones número de sesiones que ha realizado el cliente
	 */

	public int getNumeroSesiones() {
		return numeroSesiones;
	}

	/**
	 * metodo para cambiar el valor de número de sesiones que ha realizado el
	 * cliente
	 * 
	 * @param numeroSesiones número de sesiones realizadas del cliente
	 */
	public void setNumeroSesiones(int numeroSesiones) {
		this.numeroSesiones = numeroSesiones;
	}

	/**
	 * metodo que muestra todos los datos de un cliente
	 */
	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", email=" + email + ", fechaNacimiento=" + fechaNacimiento + ", usuario="
				+ usuario + ", contrasena=" + contrasena + ", activo=" + activo + ", numeroSesiones=" + numeroSesiones
				+ "]";
	}
}
