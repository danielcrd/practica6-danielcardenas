package clases;

public class Sesion {

	/**
	 * @author Daniel C�rdenas Perales
	 */

	/**
	 * Ponemos los atributos de la sesi�n: nombre, estado, nombreFisio = String
	 * precio = int cupon = boolean cliente = Cliente el modificador de visibilidad
	 * que ponemos a los atributos es protected, ya que lo usaremos en las subclases
	 * de SesionIndividual y SesionGrupal
	 */

	protected String nombre;
	protected String estado;
	protected float precio;
	protected String nombreFisio;
	protected boolean cupon;
	protected Cliente cliente;

	/**
	 * Constructor de la Sesion, se debe introducir el nombre y el estado para poder
	 * crear un objeto
	 * 
	 * @param nombre nombre de la sesi�n
	 * @param estado estado de la sesi�n
	 */

	public Sesion(String nombre, String estado) {
		this.nombre = nombre;
		this.estado = estado;
	}

	/**
	 * contrsuctor de la sesi�n, en este se tendra que introducir todos los
	 * atributos para crearlo.
	 * 
	 * @param nombre      nombre de la sesi�n
	 * @param estado      estado de la sesi�n
	 * @param precio      precio de la sesi�n
	 * @param nombreFisio nombre del Fisio que realizara la sesi�n
	 * @param cupon       booleano para comprobar si la sesion tiene un cupon o no
	 * 
	 */

	public Sesion(String nombre, String estado, float precio, String nombreFisio, boolean cupon) {
		this.nombre = nombre;
		this.estado = estado;
		this.precio = precio;
		this.nombreFisio = nombreFisio;
		this.cupon = cupon;
	}

	/**
	 * Metodo que nos devuelve el nombre de la sesi�n
	 * 
	 * @return un String con el nombre
	 */

	public String getNombre() {
		return nombre;
	}

	/**
	 * metodo para cambiar el nombre de la sesi�n
	 * 
	 * @param nombre nombre de la sesi�n
	 */

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * metodo el cual nos devuelve el estado de una sesi�n
	 * 
	 * @return un String con el estado de la sesi�n
	 */

	public String getEstado() {
		return estado;
	}

	/**
	 * metodo para cambiar el estado de una sesi�n
	 * 
	 * @param estado el estado de la sesi�n
	 */

	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * metodo que devuelve el precio de una sesi�n
	 * 
	 * @return un int con el valor de la sesi�n
	 */

	public float getPrecio() {
		return precio;
	}

	/**
	 * metodo que cambia el valor de una sesi�n
	 * 
	 * @param precio precio de la sesi�n llamada
	 */

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	/**
	 * metodo que nos devuelve el nombre del fisio que hara la sesi�n
	 * 
	 * @return un String con el nombre del fisio
	 */

	public String getNombreFisio() {
		return nombreFisio;
	}

	/**
	 * metodo que cambia el nombre del fisio por el valor introducido
	 * 
	 * @param nombreFisio nombre del fisio que realizara la sesi�n
	 */

	public void setNombreFisio(String nombreFisio) {
		this.nombreFisio = nombreFisio;
	}

	/**
	 * metodo que devuelve un booleano diciendo si tiene un cup�n la sesi�n o no
	 * 
	 * @return un booleano con el valor de true o false dependiendo de la sesi�n
	 */

	public boolean isCupon() {
		return cupon;
	}

	/**
	 * metodo para cambiar el valor del atributo cupon, para comprobar si tiene o no
	 * un cup�n la sesi�n
	 * 
	 * @param cupon valor para diferenciar si tiene un cup�n o no
	 */

	public void setCupon(boolean cupon) {
		this.cupon = cupon;
	}

	/**
	 * metodo el cual nos devolvera el cliente asignado a la sesi�n llamada
	 * 
	 * @return un Cliente con todos los valores de su clase
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * Metodo el cual podremos cambiar a otro cliente introducido para asignarle a
	 * una sesi�n
	 * 
	 * @param cliente un objeto cliente que se asignara a una sesi�n
	 */

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * metodo en el cual podremos subir el precio a la sesion
	 * 
	 * @param precio precio que vayamos a�adir al precio total
	 */

	public void subidaPrecio(float precio) {
		this.precio += precio;
	}

	/**
	 * metodo en el cual podremos bajar el precio a la sesion
	 * 
	 * @param precio2 precio que vayamos a bajar al precio total
	 */

	public void bajadaPrecio(float precio2) {
		this.precio -= precio2;
	}

	/**
	 * metodo el cual devolvera todos los valores de cada uno de los atributos
	 * asignados
	 */

	@Override
	public String toString() {
		return "Sesion [nombre=" + nombre + ", estado=" + estado + ", precio=" + precio + ", nombreFisio=" + nombreFisio
				+ ", cupon=" + cupon + ", cliente=" + cliente + "]";
	}

}
