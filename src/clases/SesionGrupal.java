package clases;

import java.util.ArrayList;

public class SesionGrupal extends Sesion {

	/**
	 * @author Daniel C�rdenas Perales
	 */

	/**
	 * Atributos de la sesi�n Grupal, todos con el modificador de visibilidad
	 * private, para sacar o cambiar los valores se necesitara utilizar los metodos
	 * getter and setter. aforoMaximoSala y numeroMaterialesTotal = int
	 * segundoNombreFisio = String
	 */

	private String segundoNombreFisio;
	private int aforoMaximoSala;
	private int numeroMaterialesTotal;
	private ArrayList<Cliente> listadoClientes;

	/**
	 * Creamos el constructor de sesionGrupal, con atributos especificos como
	 * segundoNombreFisio, aforoMaximoSala, numeroMaterialesTotal ademas en una
	 * subclase como es esta encontramos la palabra super que hace referencia al
	 * objeto actual de la super clase que ser�a Sesion
	 * 
	 * @param nombre                nombre de la sesi�n
	 * @param estado                estado de la sesi�n
	 * @param precio                precio de la sesi�n
	 * @param nombreFisio           nombre del encargado de hacer la sesi�n
	 * @param cupon                 si tiene o no cupon la sesi�n
	 * @param cliente               cliente asignado a la sesi�n
	 * @param segundoNombreFisio2   segundo nombre del Fisio que realizara la sesi�n
	 * @param aforoMaximoSala       n�mero de clientes que pueden estar en una sala
	 * @param numeroMaterialesTotal n�mero de materiales que se usaran en la sesi�n
	 */

	public SesionGrupal(String nombre, String estado, float precio, String nombreFisio, boolean cupon,
			String segundoNombreFisio2, int aforoMaximoSala, int numeroMaterialesTotal) {
		super(nombre, estado, precio, nombreFisio, cupon);
		this.segundoNombreFisio = segundoNombreFisio2;
		this.aforoMaximoSala = aforoMaximoSala;
		this.numeroMaterialesTotal = numeroMaterialesTotal;
		this.listadoClientes = new ArrayList<Cliente>();
	}

	/**
	 * muestra el valor del atributo getSegundoNombreFisio, que hace referencia al
	 * nombre del segundo fisio
	 * 
	 * @return segundoNombreFisio
	 */

	public String getSegundoNombreFisio() {
		return segundoNombreFisio;
	}

	/**
	 * metodo para cambiar el nombre del segundo fisio
	 * 
	 * @param segundoNombreFisio
	 */

	public void setSegundoNombreFisio(String segundoNombreFisio) {
		this.segundoNombreFisio = segundoNombreFisio;
	}

	/**
	 * metodo que devuelve el valor del aforo maximo de la sala
	 * 
	 * @return aforoMaximoSala
	 */

	public int getAforoMaximoSala() {
		return aforoMaximoSala;
	}

	/**
	 * metodo que cambia el valor del atributo setAforoMaximoSala, por un valor que
	 * recibe el metodo
	 * 
	 * @param aforoMaximoSala
	 */

	public void setAforoMaximoSala(int aforoMaximoSala) {
		this.aforoMaximoSala = aforoMaximoSala;
	}

	/**
	 * metodo que muestra el valor del atributo getNumeroMaterialesTotal
	 * 
	 * @return numeroMaterialesTotal
	 */
	public int getNumeroMaterialesTotal() {
		return numeroMaterialesTotal;
	}

	/**
	 * metodo que cambia el valor del atributo setNumeroMaterialesTotal, por uno
	 * recibido por el metodo
	 * 
	 * @param numeroMaterialesTotal
	 */
	public void setNumeroMaterialesTotal(int numeroMaterialesTotal) {
		this.numeroMaterialesTotal = numeroMaterialesTotal;
	}

	/**
	 * metodo en el cual nos mostrara todos los valores asignados del objeto llamado
	 * de la clase SesionGrupal
	 */

	@Override
	public String toString() {
		return "SesionGrupal [segundoNombreFisio=" + segundoNombreFisio + ", aforoMaximoSala=" + aforoMaximoSala
				+ ", numeroMaterialesTotal=" + numeroMaterialesTotal + ", nombre=" + nombre + ", estado=" + estado
				+ ", precio=" + precio + ", nombreFisio=" + nombreFisio + ", cupon=" + cupon + "]";
	}

	/**
	 * metodo en el cual nos mostrara cuantos clientes pueden registrarse en la
	 * sesi�n, restando los clientes registrados con el total de afoto de la sala.
	 * 
	 * @param numero numero de clientes registrados
	 */

	public void numeroClientesSesion() {
		int resultado = 0;
		resultado = this.aforoMaximoSala - listadoClientes.size();
		System.out.println("Se pueden inscribir un total de clientes: " + resultado + " en la sala");
	}

	/**
	 * metodo el cual podremos dar de alta a una sesi�n grupal, un cliente
	 * introducido al llamar al metodo
	 * 
	 * @param cliente objeto que hace referencia al cliente
	 */
	public void altaClienteSesionGrupal(Cliente cliente) {
		listadoClientes.add(cliente);
	}

	/**
	 * metodo el cual podremos listar todos los clientes que se han dado de alta en
	 * una sesi�n grupal, si no hay ning�n cliente en dicha sesi�n, nos mostrara el
	 * mensaje correcto
	 */
	public void listarClienteSesionGrupal() {
		for (Cliente cliente : listadoClientes) {
			if (cliente != null) {
				System.out.println(cliente);
			}

		}
		if (listadoClientes.size() == 0) {
			System.out.println("No hay clientes registrados en esta sesi�n");
		}
	}

}
