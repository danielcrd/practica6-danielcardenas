package programa;

import java.util.InputMismatchException;
import java.util.Scanner;

import clases.GestorSesionCliente;

public class Programa {

	static Scanner input = new Scanner(System.in);
	static GestorSesionCliente gestorFisio = new GestorSesionCliente();

	public static void main(String[] args) {

		System.out.println("Alta de cada una de las Clases");
		gestorFisio.altaCliente("Daniel", "daniel@daniel.com", "1998-09-06", "dani", "1234", 1);
		gestorFisio.altaCliente("Marcos", "marcos@daniel.com", "2000-09-06", "marcos", "1234", 1);
		gestorFisio.altaSesion("sesion", "confirmado", 345.45f, "Alberto", false);
		gestorFisio.altaSesion("sesion1", "confirmado", 345.45f, "Alberto", false);
		gestorFisio.altaSesionGrupal("sesionGrupal", "pendiente", 290.23f, "Alberto", false, "Miguel", 10, 20);
		gestorFisio.altaSesionGrupal("sesionGrupal1", "error", 294.23f, "Alberto", false, "Miguel", 10, 20);
		gestorFisio.altaSesionIndividual("sesionIndividual", "pendiente", 100f, "Miguel", true, false, false);
		gestorFisio.altaSesionIndividual("sesionIndividual1", "error", 256.23f, "Alberto", true, false, false);
		System.out.println("*******************************************************************************");
		System.out.println("Buscar elementos de cada Clase");
		System.out.println(gestorFisio.buscarCliente("dani"));
		System.out.println(gestorFisio.buscarSesion("sesion"));
		System.out.println(gestorFisio.buscarSesionIndividual("sesionIndividual"));
		System.out.println(gestorFisio.buscarSesionGrupal("sesionGrupal"));
		System.out.println("*******************************************************************************");
		System.out.println("Listar elementos de cada Clase");
		gestorFisio.listarSesion();
		System.out.println("*******************************************************************************");
		gestorFisio.listarSesionIndividual();
		System.out.println("*******************************************************************************");
		gestorFisio.listarSesionGrupal();
		System.out.println("*******************************************************************************");
		gestorFisio.listarCliente();
		System.out.println("*******************************************************************************");
		System.out.println("Eliminar elementos de cada Clase");
		gestorFisio.eliminarCliente("dani");
		gestorFisio.listarCliente();
		System.out.println("***********");
		gestorFisio.eliminarSesion("sesion");
		gestorFisio.listarSesion();
		System.out.println("***************");
		gestorFisio.eliminarSesionIndivual("sesionIndividual");
		gestorFisio.listarSesionIndividual();
		System.out.println("***************");
		gestorFisio.eliminarSesionGrupal("sesionGrupal");
		gestorFisio.listarSesionGrupal();
		System.out.println("*******************************************************************************");

		System.out.println("");
		menuPrincipal();
	}

	public static void menuPrincipal() {
		int opcion;
		do {
			System.out.println("******************************************************");
			System.out.println("��Bienvenido al programa de gesti�n de fisioterapia!!");
			System.out.println("1.- Control de Clientes");
			System.out.println("2.- Control de sesiones");
			System.out.println("3.- Control de sesiones Individuales");
			System.out.println("4.- Control de sesiones Grupales");
			System.out.println("5.- Salir");
			System.out.println("******************************************************");
			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				System.out.println("Control de Clientes");
				menuCliente();
				break;
			case 2:
				System.out.println("Control de Sesiones");
				menuSesion();
				break;
			case 3:
				System.out.println("Control de Sesiones Individuales");
				menuSesionIndividual();
				break;
			case 4:
				System.out.println("Control de Sesiones Grupales");
				menuSesionGrupal();
				break;
			case 5:
				System.out.println("Salir");
				System.out.println("Gracias por utilizarme!!");
				break;
			default:
				System.out.println("Opci�n no valida");
				break;
			}
		} while (opcion != 5);
	}

	public static void menuCliente() {
		int ocpionCliente;
		do {
			System.out.println("******************************************************");
			System.out.println("1.- Dar de alta un cliente");
			System.out.println("2.- Dar de baja un cliente");
			System.out.println("3.- Buscar un cliente");
			System.out.println("4.- listar todos los clientes");
			System.out.println("5.- Salir del menu de cliente");
			System.out.println("******************************************************");
			ocpionCliente = input.nextInt();
			input.nextLine();
			String usuario;

			switch (ocpionCliente) {
			case 1:
				altaCliente();
				break;
			case 2:
				System.out.println("Por favor introduzca el usuario del cliente:");
				usuario = input.nextLine();
				gestorFisio.eliminarCliente(usuario);
				break;
			case 3:
				System.out.println("Por favor introduzca el usuario del cliente:");
				usuario = input.nextLine();
				if (gestorFisio.buscarCliente(usuario) == null) {
					System.out.println("El cliente no existe");
				} else {
					System.out.println(gestorFisio.buscarCliente(usuario));
				}
				break;
			case 4:
				gestorFisio.listarCliente();
				break;
			case 5:
				System.out.println("Has elegido salir del men� cliente");
				break;
			default:
				System.out.println("Opci�n no valida");
				break;
			}
		} while (ocpionCliente != 5);

	}

	public static void menuSesion() {
		int opcionSesion;
		do {
			System.out.println("******************************************************");
			System.out.println("1.- Dar de alta una sesi�n");
			System.out.println("2.- Dar de baja una sesi�n");
			System.out.println("3.- Buscar una sesi�n");
			System.out.println("4.- listar todas las sesiones");
			System.out.println("5.- Asignar una sesi�n a un cliente");
			System.out.println("6.- Subir o bajar el precio de una sesi�n");
			System.out.println("7.- Salir del menu de Sesi�n");
			System.out.println("******************************************************");
			opcionSesion = input.nextInt();
			input.nextLine();
			String nombre;

			switch (opcionSesion) {
			case 1:
				altaSesion();
				break;
			case 2:
				System.out.println("Por favor introduzca el nombre de la sesi�n:");
				nombre = input.nextLine();
				gestorFisio.eliminarSesion(nombre);
				break;
			case 3:
				System.out.println("Por favor introduzca el nombre de la sesi�n:");
				nombre = input.nextLine();
				if (gestorFisio.buscarSesion(nombre) == null) {
					System.out.println("No existe la sesi�n");
				} else {
					System.out.println(gestorFisio.buscarSesion(nombre));
				}
				break;
			case 4:
				gestorFisio.listarSesion();
				break;
			case 5:
				System.out.println("Por favor introduzca el nombre de la sesi�n:");
				nombre = input.nextLine();
				System.out.println("Por favor introduzca el usuario del cliente");
				String usuario = input.nextLine();
				gestorFisio.asignarSesionCliente(usuario, nombre);
				break;
			case 6:
				precioSesion();
				break;
			case 7:
				System.out.println("Has elegido salir del men� sesi�n");
				break;
			default:
				System.out.println("Opci�n no valida");
				break;
			}
		} while (opcionSesion != 7);
	}

	public static void menuSesionIndividual() {
		int opcionSesionIndividual;
		do {
			System.out.println("******************************************************");
			System.out.println("1.- Dar de alta una sesi�n individual");
			System.out.println("2.- Dar de baja una sesi�n individual");
			System.out.println("3.- Buscar una sesi�n individual");
			System.out.println("4.- listar todas las sesiones individuales");
			System.out.println("5.- Asignar una sesi�n individual a un cliente");
			System.out.println("6.- Subir o bajar el precio de una sesi�n individual");
			System.out.println("7.- Salir del menu de Sesi�n Individual");
			System.out.println("******************************************************");
			opcionSesionIndividual = input.nextInt();
			input.nextLine();
			String nombre;

			switch (opcionSesionIndividual) {
			case 1:
				altaSesionIndividual();
				break;
			case 2:
				System.out.println("Por favor introduzca el nombre de la sesi�n individual:");
				nombre = input.nextLine();
				gestorFisio.eliminarSesionIndivual(nombre);
				break;
			case 3:
				System.out.println("Por favor introduzca el nombre de la sesi�n individual:");
				nombre = input.nextLine();
				if (gestorFisio.buscarSesionIndividual(nombre) == null) {
					System.out.println("Sesi�n Individual no existe");
				} else {
					System.out.println(gestorFisio.buscarSesionIndividual(nombre));
				}
				break;
			case 4:
				gestorFisio.listarSesionIndividual();
				break;
			case 5:
				System.out.println("Por favor introduzca el nombre de la sesi�n individual:");
				nombre = input.nextLine();
				System.out.println("Por favor introduzca el usuario del cliente");
				String usuario = input.nextLine();
				gestorFisio.asignarSesionIndividualCliente(usuario, nombre);
				break;
			case 6:
				precioSesionIndividual();
				break;
			case 7:
				System.out.println("Has elegido salir del men� sesi�n individual");
				break;
			default:
				System.out.println("Opci�n no valida");
				break;
			}
		} while (opcionSesionIndividual != 7);
	}

	public static void menuSesionGrupal() {
		int opcionSesionGrupal;
		do {
			System.out.println("******************************************************");
			System.out.println("1.- Dar de alta una sesi�n grupal");
			System.out.println("2.- Dar de baja una sesi�n grupal");
			System.out.println("3.- Buscar una sesi�n grupal");
			System.out.println("4.- listar todas las sesiones grupales");
			System.out.println("5.- Asignar una sesi�n grupal a un cliente");
			System.out.println("6.- Subir o bajar el precio de una sesi�n Grupal");
			System.out.println("7.- Listar todos los clientes de una sesi�n grupal");
			System.out.println("8.- Cuantas plazas hay disponibles para inscribirse en una sesi�n grupal");
			System.out.println("9.- Salir del menu de Sesi�n Grupal");
			System.out.println("******************************************************");
			opcionSesionGrupal = input.nextInt();
			input.nextLine();
			String nombre;

			switch (opcionSesionGrupal) {
			case 1:
				altaSesionGrupal();
				break;
			case 2:
				System.out.println("Por favor introduzca el nombre de la sesi�n grupal:");
				nombre = input.nextLine();
				gestorFisio.eliminarSesionGrupal(nombre);
				break;
			case 3:
				System.out.println("Por favor introduzca el nombre de la sesi�n grupal:");
				nombre = input.nextLine();
				if (gestorFisio.buscarSesionGrupal(nombre) == null) {
					System.out.println("La sesi�n grupal no existe");
				} else {
					System.out.println(gestorFisio.buscarSesionGrupal(nombre));
				}
				break;
			case 4:
				gestorFisio.listarSesionGrupal();
				break;
			case 5:
				System.out.println("Por favor introduzca el nombre de la sesi�n grupal:");
				nombre = input.nextLine();
				System.out.println("Por favor introduzca el usuario del cliente");
				String usuario = input.nextLine();
				gestorFisio.asignarSesionGrupalCliente(usuario, nombre);
				break;
			case 6:
				precioSesionGrupal();
				break;
			case 7:
				System.out.println("Por favor introduzca el nombre de la sesi�n grupal:");
				nombre = input.nextLine();
				gestorFisio.listarClientesSesionGrupal(nombre);
				break;
			case 8:
				System.out.println("Por favor introduzca el nombre de la sesi�n grupal:");
				nombre = input.nextLine();
				gestorFisio.clientesDisponiblesPorSesionGrupal(nombre);
				break;
			case 9:
				System.out.println("Has elegido salir del men� sesi�n grupal");
				break;
			default:
				System.out.println("Opci�n no valida");
				break;
			}
		} while (opcionSesionGrupal != 9);
	}

	public static void altaCliente() {
		System.out.println("Por favor introduzca el nombre del cliente: ");
		String nombre = input.nextLine();
		System.out.println("Por favor introduzca el correo del cliente: ");
		String email = input.nextLine();
		System.out.println("Por favor introduzca la fecha de nacimiento del cliente: ");
		String fechaNacimiento = input.nextLine();
		System.out.println("Por favor introduzca el usuario del cliente: ");
		String usuario = input.nextLine();
		System.out.println("Por favor introduzca la contrase�a del cliente: ");
		String contrasena = input.nextLine();

		boolean confirmacion = false;
		do {
			try {
				System.out.println("Por favor introduzca el n�mero de sesiones del cliente: ");
				int numeroSesiones = input.nextInt();
				gestorFisio.altaCliente(nombre, email, fechaNacimiento, usuario, contrasena, numeroSesiones);
				confirmacion = false;
			} catch (InputMismatchException e) {
				System.out.println("Caracter introducido no valido");
				confirmacion = true;
				input.nextLine();
			} catch (Exception e) {
				System.out.println("Algo va mal");
				confirmacion = true;
				input.nextLine();
			}
		} while (confirmacion);
		confirmacion = false;
	}

	public static void altaSesion() {
		System.out.println("Por favor introduzca el nombre de la sesi�n: ");
		String nombre = input.nextLine();
		System.out.println("Por favor introduzca el estado de la sesi�n: ");
		String estado = input.nextLine();
		float precio = 0;
		boolean comprobacion = false;
		do {
			try {
				System.out.println("Por favor introduzca el precio de la sesi�n: ");
				precio = input.nextFloat();
				comprobacion = false;
			} catch (InputMismatchException e) {
				System.out.println("Caracter introducido no valido");
				comprobacion = true;
				input.nextLine();
			} catch (Exception e) {
				System.out.println("Algo no funciona bien");
				comprobacion = true;
				input.nextLine();
			}
		} while (comprobacion);
		comprobacion = false;

		input.nextLine();
		System.out.println("Por favor introduzca el nombre del Fisio que realiza la sesi�n: ");
		String nombreFisio = input.nextLine();
		System.out.println("Esta sesi�n tiene un cup�n, por favor escriba si o no? ");
		String respuesta = input.nextLine();
		boolean cupon;
		if (respuesta.equalsIgnoreCase("Si")) {
			cupon = true;
			gestorFisio.altaSesion(nombre, estado, precio, nombreFisio, cupon);
		} else if (respuesta.equalsIgnoreCase("no")) {
			cupon = false;
			gestorFisio.altaSesion(nombre, estado, precio, nombreFisio, cupon);
		} else {
			System.out.println("Opci�n no valida");
		}
	}

	public static void altaSesionIndividual() {
		System.out.println("Por favor introduzca el nombre de la sesi�n: ");
		String nombre = input.nextLine();
		System.out.println("Por favor introduzca el estado de la sesi�n: ");
		String estado = input.nextLine();
		System.out.println("Por favor introduzca el precio de la sesi�n: ");
		float precio = input.nextFloat();
		input.nextLine();
		System.out.println("Por favor introduzca el nombre del Fisio que realiza la sesi�n: ");
		String nombreFisio = input.nextLine();

		System.out.println(
				"Por favor introduzca si la sesi�n es a domicilio o no? las respuestas solo pueden ser si o no");
		String respuestaDomicilio = input.nextLine();
		boolean sesionDomicilio = false;
		String direccion = "";
		if (respuestaDomicilio.equalsIgnoreCase("Si")) {
			sesionDomicilio = true;
			direccion = asignarDireccionDomicilio();
		} else if (respuestaDomicilio.equalsIgnoreCase("no")) {
			sesionDomicilio = false;
		} else {
			System.out.println(
					"Opci�n no valida, al introducir un valor incorrecto se le asigna como que no es una sesion a domicilio");
		}

		System.out.println(
				"Por favor introduzca si la sesi�n es de un cliente prime o no? las respuestas solo pueden ser si o no");
		String respuestaPrime = input.nextLine();
		boolean esPrime = false;
		if (respuestaPrime.equalsIgnoreCase("Si")) {
			esPrime = true;
		} else if (respuestaDomicilio.equalsIgnoreCase("no")) {
			esPrime = false;
		} else {
			System.out.println(
					"Opci�n no valida, al introducir un valor incorrecto se le asigna como una sesi�n no prime");
		}

		System.out.println("Esta sesi�n tiene un cup�n, por favor escriba si o no? ");
		String respuesta = input.nextLine();
		boolean cupon = false;
		if (respuesta.equalsIgnoreCase("Si")) {
			cupon = true;
		} else if (respuesta.equalsIgnoreCase("no")) {
			cupon = false;
		} else {
			System.out.println(
					"Opci�n no valida, al introducir un valor incorrecto se le asigna que dicha sesi�n no tiene cup�n");
		}

		if (sesionDomicilio) {
			gestorFisio.altaSesionIndividual(nombre, estado, precio, nombreFisio, cupon, sesionDomicilio, esPrime,
					direccion);
		} else {
			gestorFisio.altaSesionIndividual(nombre, estado, precio, nombreFisio, cupon, sesionDomicilio, esPrime);
		}
	}

	public static void altaSesionGrupal() {
		System.out.println("Por favor introduzca el nombre de la sesi�n: ");
		String nombre = input.nextLine();
		System.out.println("Por favor introduzca el estado de la sesi�n: ");
		String estado = input.nextLine();
		System.out.println("Por favor introduzca el precio de la sesi�n: ");
		float precio = input.nextFloat();
		input.nextLine();
		System.out.println("Por favor introduzca el nombre del Fisio que realiza la sesi�n: ");
		String nombreFisio = input.nextLine();
		System.out.println("Por favor introduzca el segundo nombre del Fisio que realiza la sesi�n: ");
		String segundoNombreFisio = input.nextLine();
		System.out.println("Por favor introduzca el aforo maximo de la sala");
		int aforoMaximoSala = input.nextInt();
		System.out.println("Por favor introduzca el numero de materiales de la sesi�n");
		int numeroMaterialesTotal = input.nextInt();
		input.nextLine();
		System.out.println("Esta sesi�n tiene un cup�n, por favor escriba si o no? ");
		String respuesta = input.nextLine();
		boolean cupon;
		if (respuesta.equalsIgnoreCase("Si")) {
			cupon = true;
			gestorFisio.altaSesionGrupal(nombre, estado, precio, nombreFisio, cupon, segundoNombreFisio,
					aforoMaximoSala, numeroMaterialesTotal);
		} else if (respuesta.equalsIgnoreCase("no")) {
			cupon = false;
			gestorFisio.altaSesionGrupal(nombre, estado, precio, nombreFisio, cupon, segundoNombreFisio,
					aforoMaximoSala, numeroMaterialesTotal);
		} else {
			System.out.println("Opci�n no valida");
		}
	}

	public static void precioSesion() {
		float precio;
		System.out.println("Por favor introduzca el nombre de la sesi�n:");
		String nombre = input.nextLine();
		System.out.println(
				"Si quiere subir de precio selecione el n�mero 1, si quiere bajar el precio selecione el n�mero 2");
		int opcionPrecio = input.nextInt();
		if (opcionPrecio == 1) {
			System.out.println("Por favor introduzca un n�mero positivo");
			precio = input.nextFloat();
			gestorFisio.subirPrecioSesion(nombre, precio);
		} else if (opcionPrecio == 2) {
			System.out.println("Por favor introduzca un n�mero positivo");
			precio = input.nextFloat();
			gestorFisio.bajarPrecioSesion(nombre, precio);
		} else {
			System.out.println("Opci�n no valida");
		}
	}

	public static void precioSesionIndividual() {
		float precio;
		System.out.println("Por favor introduzca el nombre de la sesi�n:");
		String nombre = input.nextLine();
		System.out.println(
				"Si quiere subir de precio selecione el n�mero 1, si quiere bajar el precio selecione el n�mero 2");
		int opcionPrecio = input.nextInt();
		if (opcionPrecio == 1) {
			System.out.println("Por favor introduzca un n�mero positivo");
			precio = input.nextFloat();
			gestorFisio.subirPrecioSesionIndividual(nombre, precio);
		} else if (opcionPrecio == 2) {
			System.out.println("Por favor introduzca un n�mero positivo");
			precio = input.nextFloat();
			gestorFisio.bajarPrecioSesionIndividual(nombre, precio);
		} else {
			System.out.println("Opci�n no valida");
		}
	}

	public static void precioSesionGrupal() {
		float precio;
		System.out.println("Por favor introduzca el nombre de la sesi�n:");
		String nombre = input.nextLine();
		System.out.println(
				"Si quiere subir de precio selecione el n�mero 1, si quiere bajar el precio selecione el n�mero 2");
		int opcionPrecio = input.nextInt();
		if (opcionPrecio == 1) {
			System.out.println("Por favor introduzca un n�mero positivo");
			precio = input.nextFloat();
			gestorFisio.subirPrecioSesionGrupal(nombre, precio);
		} else if (opcionPrecio == 2) {
			System.out.println("Por favor introduzca un n�mero positivo");
			precio = input.nextFloat();
			gestorFisio.bajarPrecioSesionGrupal(nombre, precio);
		} else {
			System.out.println("Opci�n no valida");
		}
	}

	public static String asignarDireccionDomicilio() {
		System.out.println("Por favor introduzca la direccion del cliente");
		String direccion = input.nextLine();
		return direccion;
	}
}
